let userName = getUserName();
let age = getAge();

const EIGHTEEN = 18;
const TWENTY_TWO = 22;

if (isUserValid()) {

    if (age < EIGHTEEN) {
        blockUser();
    }
    if (age > EIGHTEEN && age <= TWENTY_TWO) {
        isAllowedByUser() ? authorizeUser() : blockUser();
    }
    if (age > TWENTY_TWO) {
        authorizeUser();
    }
} else {
    alert("Validation error")
}

function blockUser() {
    alert("You are not allowed to visit this website.");
}

function authorizeUser() {
    alert(`Welcome, ${userName}!`);
}

function isAllowedByUser() {
    return confirm("Are you sure you want to continue?");
}

function getUserName() {
    return prompt("Please enter your name: ");
}

function getAge() {
    return Number.parseInt(prompt("Please enter your age: "));
}

function isUserValid() {

    if (userName == null || userName.length == 0 || isNaN(age) || age < 0) {

        alert("Something went wrong. Please try again");
        userName = getUserName();
        age = getAge();
        if (userName == null || userName.length == 0) {
            return false;
        } else if (age == null || isNaN(age) || age < 0) {
            return false;
        }
    } else {
        return true;
    }
}


