function createNewUser() {
    let result = {
        getLogin: () => {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        getAge: () => {
            return this.age;
        },
        getPassword: () => {
            const pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
            return this.firstName.charAt(0).toUpperCase()
                + this.lastName.toLowerCase()
                + new Date(this.age.replace(pattern,'$3-$2-$1')).getFullYear();
        }
    };

    //FIXME refactor - introduce defineProperty function, reuse code;
    Object.defineProperty(result, 'firstName', {
        get: () => {
            return this.firstName;
        },
        set: (firstName) => {
            this.firstName = firstName;
        }
    });

    Object.defineProperty(result, 'lastName', {
        get: () => {
            return this.lastName;
        },
        set: (lastName) => {
            this.lastName = lastName;
        }
    });

    Object.defineProperty(result, 'age', {
        get: () => {
            return this.age;
        },
        set: (age) => {
            this.age = age;
        }
    });

    let firstName = prompt("Please enter first name");
    let lastName = prompt("Please enter last name");
    let age = prompt("Please enter your age in format - dd.mm.yyyy");

    result.firstName = firstName;
    result.lastName = lastName;
    result.age = age;

    return result;
}

let newUser = createNewUser();

console.log(`Login for user ${newUser.firstName} ${newUser.lastName}, age:${newUser.getAge()} is: ${newUser.getLogin()}, password is: ${newUser.getPassword()}`);