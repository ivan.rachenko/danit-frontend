function createNewUser() {
    let result = {
        getLogin: () => {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
    };

    Object.defineProperty(result, 'firstName', {
        get: () => {
            return this.firstName;
        },
        set: (firstName) => {
            this.firstName = firstName;
        }
    });

    Object.defineProperty(result, 'lastName', {
        get: () => {
            return this.lastName;
        },
        set: (lastName) => {
            this.lastName = lastName;
        }
    });

    let firstName = prompt("Please enter first name");
    let lastName = prompt("Please enter last name");

    result.firstName = firstName;
    result.lastName = lastName;

    return result;
}

let newUser = createNewUser();

console.log(`Login of user ${newUser.firstName} ${newUser.lastName} is: ${newUser.getLogin()}`);