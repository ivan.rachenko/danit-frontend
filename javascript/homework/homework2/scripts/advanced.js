let firstNumber = Number.parseInt(prompt("Please enter first number: "));
let secondNumber = Number.parseInt(prompt("Please enter second number: "));

if (!isNaN(firstNumber) && !isNaN(secondNumber)) {

    let m = Math.min(firstNumber, secondNumber);
    let n = Math.max(firstNumber, secondNumber);
    console.log(`Writing simple numbers in range from ${m} to ${n}: `);
    while (m <= n) {
        if (isSimple(m)) {
            console.log(m);
        }
        m++;
    }
} else {
    console.log("Validation error");
}

function isSimple(num) {

    num = parseInt(num);
    if (num > 1 && num <= 3) {
        return true;
    }
    if (String(num).substr(String(num).length - 1) === "0" || isEven(num) || num === 1 || num === 0) {
        return false;
    }
    for (let x = 3; x <= Math.sqrt(num); x++) {
        if (num % x === 0) {
            return false;
        }
    }
    return true;
}

function isEven(num) {
    return num % 2 === 0 ? true : false;
}