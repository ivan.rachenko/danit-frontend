const ZERO = 0;
const FIVE = 5;

let counter = ZERO;
let input = +prompt("Please enter your number");

if (!isInteger(input)) {

    alert(`Input value: ${input} is not an Integer. Please try again`);

    while (true) {
        input = +prompt("Please enter your number again");
        if (isInteger(input)) {
            break;
        }
        alert(`Input value: ${input} is not an Integer. Please try again`);
    }
}

console.log(`Writing numbers that are aliquot to 5 fetched from number -> Abs(${input}):`);

while (counter <= Math.abs(input)) {

    if (counter % FIVE == ZERO && counter != ZERO) {
        console.log(counter);
    }
    if (Math.abs(input) < FIVE) {
        console.log("Sorry, no numbers");
    }
    counter++;
}

function isInteger(num) {
    return (num ^ ZERO) === num;
}


